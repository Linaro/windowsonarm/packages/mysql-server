rem ******************************************************************************
rem USAGE:
rem   build.bat version
rem
rem Where version in a git revision to checkout for mysql-server
rem ******************************************************************************
rem It is assumed that you have the following installed on your system:
rem   Visual Studio 2022 (17.3 or higher) incl 
rem     22000 SDK or higher
rem     ARM64 libs
rem   Perl
rem   Git
rem   
rem It is also assumed that the following is true
rem   You are using an ARM64 host machine
rem   You want a debug build (as most tests are disabled in release mode)
rem ******************************************************************************
rem In place of the equivalent of "set -e" in a shell script, we use "|| exit 1"
rem ******************************************************************************

@echo on
setlocal

set MYSQL_VERSION=%1

rem Set the various locations required by the script
set MYSQL_SERVER_DIR=%CD%\mysql-server
set MYSQL_SERVER_BUILD_DIR=%MYSQL_SERVER_DIR%\build
set OPENSSL_DIR=%CD%\openssl
set OPENSSL_INSTALL_DIR=%CD%\openssl-install
set WINFLEXBISON_DIR=%CD%\winflexbison
set WINFLEXBISON_INSTALL_DIR=%CD%\winflexbison-install
set VCPKG_DIR=%CD%\vcpkg
set VCPKG_LIBEVENT_DIR=%VCPKG_DIR%\packages\libevent_arm64-windows
set BOOST_DIR=%CD%\boost-mysql

rem Bump system32 back up to the front of the queue so as to use it's tar instead of the msys2 one
set PATH=C:\Windows\System32;%PATH%

if not exist "%MYSQL_SERVER_DIR%" (
  echo Cloning mysql-server
  git clone -b %MYSQL_VERSION% --depth 1 --single-branch https://github.com/mysql/mysql-server %MYSQL_SERVER_DIR% || exit 1
  
  rem Apply patches (not using am, as it errors out if no user is set for git, ie in a CI machine with a fresh env)
  pushd %MYSQL_SERVER_DIR%
  git apply --ignore-whitespace %~dp0\0001-Add-secondary-method-of-checking-libevent-version.patch || exit 1
  git apply --ignore-whitespace %~dp0\0002-Silence-C5257-warning.patch || exit 1
  git apply --ignore-whitespace %~dp0\0003-Add-support-for-Windows-ARM64-builds.patch || exit 1
  git apply --ignore-whitespace %~dp0\0004-Define-minimum-version-for-ARM64-Windows.patch || exit 1
  git apply --ignore-whitespace %~dp0\0005-Add-check-for-clang-on-Windows-ARM64-platforms.patch || exit 1
  popd
)

if not exist "%OPENSSL_DIR%" (
  echo "Cloning openssl"
  git clone -b OpenSSL_1_1_1-stable --depth 1 --single-branch https://github.com/openssl/openssl %OPENSSL_DIR% || exit 1
)

if not exist %WINFLEXBISON_DIR% (
  echo Cloning winflexbison
  git clone -b master --depth 1 --single-branch https://github.com/lexxmark/winflexbison %WINFLEXBISON_DIR% || exit 1
)

rem We assume that if the vcpkg directory exists, it has been bootstrapped
if not exist "%VCPKG_DIR%" (
  echo Cloning vcpkg
  git clone -b master --depth 1 --single-branch https://github.com/microsoft/vcpkg %VCPKG_DIR% || exit 1
  pushd %VCPKG_DIR%
  call .\bootstrap-vcpkg.bat -disableMetric
  @echo on
  popd
)

rem Install libevent into vcpkg if it doesn't exist
if not exist "%VCPKG_LIBEVENT_DIR%" (
  pushd %VCPKG_DIR%
  vcpkg.exe install libevent:arm64-windows || exit 1
  popd
)

rem Fix Debug DLL Path
rem This fixes errors in ARM64 when using debug builds. See for more info: https://linaro.atlassian.net/wiki/spaces/WOAR/pages/28677636097/Debug+run-time+DLL+issue
set PATH=%PATH%;%WindowsSdkVerBinPath%\arm64\ucrt;%VCToolsRedistDir%\onecore\debug_nonredist\arm64\Microsoft.VC143.DebugCRT

rem Build OpenSSL, and install it into the installation directory
rem We assume that if the installation directory exists, OpenSSL has been built and installed into it already, so we skip this step
if not exist "%OPENSSL_INSTALL_DIR%" (
  mkdir %OPENSSL_INSTALL_DIR%
  pushd %OPENSSL_DIR%
  perl Configure VC-WIN64-ARM --prefix=%OPENSSL_INSTALL_DIR% --openssldir=%OPENSSL_INSTALL_DIR% --debug || exit 1
  nmake || exit 1
  nmake install || exit 1
)

rem Build WinFlexBison, and install it into the installation directory
rem We assume that if the installation directory exists, WinFlexBison has been built and installed into it already, so we skip this step
if not exist %WINFLEXBISON_INSTALL_DIR% (
  rem Create the installation dir and push into it
  mkdir %WINFLEXBISON_INSTALL_DIR%
  pushd %WINFLEXBISON_INSTALL_DIR%
  rem Switch to the source dir
  pushd %WINFLEXBISON_DIR%
  rem Build WinFlexBison (for ARM64)
  mkdir build
  cd build
  cmake .. -A ARM64 -G "Visual Studio 17 2022" || exit 1
  cmake --build . --config "Release" --target package || exit 1
  popd
  rem Go back to the installation dir and unzip the build output, and rename produced exe files
  tar -xf %WINFLEXBISON_DIR%\build\win_flex_bison-master.zip
  move win_bison.exe bison.exe
  move win_flex.exe flex.exe
  popd
)

set PATH=%PATH%;%WINFLEXBISON_INSTALL_DIR%

rem We assume that CMake has *not* been run if there is no build dir, so we run it
if not exist %MYSQL_SERVER_BUILD_DIR% (
  mkdir %MYSQL_SERVER_BUILD_DIR%
  pushd %MYSQL_SERVER_BUILD_DIR%
  
  for /f "delims=" %%i in ('where cl') do set CL_PATH=%%i

  set MYSQL_EXTRA_CMAKE_ARGS=-DCMAKE_C_COMPILER="!CL_PATH!" -DCMAKE_CXX_COMPILER="!CL_PATH!"
  cmake .. -G "Visual Studio 17 2022" -A ARM64 -DWITH_BOOST="%BOOST_DIR%" -DDOWNLOAD_BOOST=1 -DCMAKE_TOOLCHAIN_FILE="%VCPKG_DIR%\scripts\buildsystems\vcpkg.cmake" -DWITH_SSL=%OPENSSL_INSTALL_DIR% !MYSQL_EXTRA_CMAKE_ARGS! || exit 1
  popd
)

rem Now we actually build mysql-server
pushd %MYSQL_SERVER_BUILD_DIR%

if "%VSCMD_ARG_HOST_ARCH%"=="arm64" (
  msbuild /p:Configuration=Debug /p:PreferredToolArchitecture=ARM64 /m MySQL.sln || exit 1
) else (
  msbuild /p:Configuration=Debug /m MySQL.sln || exit 1
)

popd

echo Done!

endlocal
