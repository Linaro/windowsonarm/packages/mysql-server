import shutil
from os.path import dirname, join

from packagetools.builder import *
from packagetools.command import run

VERSION = "mysql-8.0.29"
RECIPE_DIR = dirname(__file__)


class Recipe(PackageShortRecipe):
    def describe(self) -> PackageInfo:
        return PackageInfo(
            description="mysql-server",
            id="mysql-server",
            pretty_name="MySQL Server",
            version=VERSION,
        )

    def all_steps(self, out_dir: str):
        build = join(RECIPE_DIR, "build.bat")
        run("cmd", "/c", build, VERSION, out_dir)


PackageBuilder(Recipe()).make()
